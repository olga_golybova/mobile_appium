import java.net.URL;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;

import org.junit.After;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SendEmail {

    private static final String SITE = "https://gmail.com";
    private static final String APPIUM = "http://0.0.0.0:4723";

    private AppiumDriver driver;

    @Before
    public void setUp() throws Exception {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "11.0");
        caps.setCapability("deviceName", "Android Emulator");
        caps.setCapability("automationName",
                "UiAutomator2");
        caps.setCapability("browserName", "Chrome");
        caps.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));

        caps.setCapability("noReset", true);
        driver = new AppiumDriver(new URL(APPIUM), caps);

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void testSendEmail() throws InterruptedException {

        driver.get(SITE);
        driver.findElement(By.xpath("//input[@type = 'email']")).sendKeys("appium6@gmail.com" + "\n");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='password']")));
        driver.hideKeyboard();
        element.sendKeys("password" + "\n");
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class = 'il d v']")));
        element.click();
        element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='composeto']")));
        element.sendKeys("amazon.test3001@gmail.com");
        element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='cmcsubj']")));
        element.sendKeys("amazon.test3001@gmail.com");
        driver.findElement(By.xpath("//div[@data-control-type='cmaasend+104']")).click();
        Thread.sleep(5000);
        element = driver.findElement(By.xpath("//div[@class='Ls']"));
        //div[@class="Ls"]
        assert element.isDisplayed();

    }
}